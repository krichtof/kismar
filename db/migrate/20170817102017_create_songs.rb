class CreateSongs < ActiveRecord::Migration[5.1]
  def change
    create_table :songs do |t|
      t.string :Song
      t.string :title
      t.string :artist
    end
  end
end
