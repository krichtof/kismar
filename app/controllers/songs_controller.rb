class SongsController < ApplicationController
  def new
    build_song
  end

  def create
    build_song
    save_song or render 'new'
  end

  def index
    load_songs
  end

  private
  def build_song
    @song ||= song_scope.build
    @song.attributes = song_params
  end

  def save_song
    if @song.save
      redirect_to songs_path
    end
  end

  def load_songs
    @songs ||= song_scope
  end

  def song_scope
    Song.all
  end

  def song_params
    song_params = params[:song]
    song_params ? song_params.permit(:title, :artist): {}
  end
end
