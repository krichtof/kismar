require 'rails_helper'

feature "home page" do
  it "affiche le cta" do
    visit root_path

    expect(page).to have_button("Explorer le répertoire Kismar")
  end
end
