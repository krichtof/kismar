require 'rails_helper'

describe SongsController do
  describe 'GET new' do
    it 'shows the new song page' do
      get :new
      expect(response).to have_http_status(:success)
      expect(response).to render_template('songs/new')
    end
  end

  describe 'POST create' do
    it 'creates a song' do
      expect do
        post :create, params: { song: { title: '1612', artist: 'Vulfpeck' } }
      end.to change { Song.count }.by(1)

      expect(response).to redirect_to(songs_path)
    end

    it 'displays error when creation is not possible' do
      post :create, params: { song: { artist: 'Garance Bauhain' } }
      expect(assigns[:song].errors).to be_present
    end
  end

  describe 'GET index' do

    it 'displays all songs' do
      songs = double("songs")
      Song.stub(:all).and_return(songs)

      get :index

      expect(response).to render_template('songs/index')
      expect(response).to have_http_status(:success)
      expect(assigns[:songs]).to eq(songs)
    end

  end
end
