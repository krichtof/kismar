# language: fr
Fonctionnalité: Page d'accueil

  Scénario: Atterir sur la page d'accueil

    Etant donné que je suis un utilisateur anonyme
    Quand je visite la page d'accueil
    Alors je vois le bouton "Explorer le répertoire Kismar"
