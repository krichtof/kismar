Etantdonnéque(/^je suis un utilisateur anonyme$/) do
end

Quand(/^je visite la page d'accueil$/) do
  visit root_path
end

Quand(/^je visite la page "([^"]*)"$/) do |page|
  within '#menu' do
    click_link page
  end
end

Quand(/^je soumets la chanson "([^"]*)" de "([^"]*)"$/) do |title, artist|
  fill_in('song[title]', with: title)
  fill_in('song[artist]', with: artist)
  click_button 'Ajouter la chanson'
end

Quand(/^je soumets une chanson sans titre$/)do
  click_button 'Ajouter la chanson'
end

Alors(/^je devrais atterrir sur la page "([^"]*)"$/) do |page|
  expect(current_path).to eq(path_for(page))
end

Alors(/^je vois le bouton "([^"]*)"$/) do |button|
  expect(page).to have_button(button)
end

Alors(/^je devrais voir la chanson "([^"]*)" de l'artiste "([^"]*)"$/) do |title, artist|
  within ".repertoire .song .title" do
    expect(page).to have_content(title)
  end
  within ".repertoire .song .artist" do
    expect(page).to have_content(artist)
  end
end

Alors(/^survient l'erreur "([^"]*)"$/) do |erreur|
  within ".errors" do
    expect(page).to have_content(erreur)
  end
end
