# language: fr
Fonctionnalité: Ajouter une chanson

  Scénario: L'utilisateur ajoute une chanson

    Etant donné que je visite la page d'accueil
    Et que je visite la page "Ajouter une chanson"
    Quand je soumets la chanson "1612" de "Vulfpeck"
    Alors je devrais atterrir sur la page "Répertoire"
    Et je devrais voir la chanson "1612" de l'artiste "Vulfpeck"

  Scénario: L'utilisateur n'indique pas le titre de la chanson à ajouter

    Etant donné que je visite la page d'accueil
    Et que je visite la page "Ajouter une chanson"
    Quand je soumets une chanson sans titre
    Alors survient l'erreur "Le titre de la chanson est obligatoire !"

